---
Name: "utility-upload-bootenv"
Description: "download and install bootenv isos."
Documentation: |
  Uses the uploadiso command to populate ISOs from utility/bootstrap-bootenvs Param

  As a helper, if caller passed in Pipeline names instead of BootEnvs, then it will
  lookup the correct bootenv to download for the Pipeline.

  NOTE: intented for trials, so only uploads the --arch amd64 isos!

  Checks to see if they are already installed before trying to upload

  Adds "iso" and the name of the requested bootenvs to the "bootstrap/completed" list
Meta:
  type: "setup"
  icon: "dot circle outline"
  color: "yellow"
  title: "Community Content"
  copyright: "RackN 2024"
ExtraClaims:
  - scope: "isos"
    action: "*"
    specific: "*"
  - scope: "bootenvs"
    action: "list,get"
    specific: "*"
  - scope: "profiles"
    action: "list,get"
    specific: "*"
  - scope: "profiles"
    action: "update"
    specific: "global"
RequiredParams:
  - utility/bootstrap-bootenvs
Templates:
  - Name: "iso_get.sh"
    Path: ""
    Contents: |
      #!/usr/bin/env bash
      {{ template "setup.tmpl" . }}

      {{ $stage := .Param "bootstrap/completed" }}
      echo "Previous Bootstrap stages: {{ $stage }}"

      {{ range $index, $bootenv := (.ParamComposeExpand "utility/bootstrap-bootenvs")}}
      echo "---------"
      BOOTENV={{$bootenv}}

      if drpcli profiles exists $BOOTENV > /dev/null 2>&1; then
        echo "  $BOOTENV is actually a ux://profiles/$BOOTENV, getting BootEnv"
        BOOTENV=$(drpcli profiles get $BOOTENV param linux/install-bootenv-override | jq -r .)
        echo "  Lookup Bootenv is $BOOTENV from linux/install-bootenv-override"
      fi

      if drpcli bootenvs exists $BOOTENV > /dev/null 2>&1; then
        echo "  Item {{$index}}: Attempting to download or update ISO for ux://bootenvs/$BOOTENV"
        file=$(drpcli bootenvs show $BOOTENV | jq -r '.OS.SupportedArchitectures.x86_64.IsoFile')
        if drpcli isos exists $file > /dev/null 2>&1; then
          echo "  Skipping: $file already uploaded for $BOOTENV"
        else
          echo "  Uploading $file for $BOOTENV ..."
          drpcli bootenvs uploadiso $BOOTENV --arch amd64
          echo "    ... done"
          {{ $stage = append $stage $bootenv }}
        fi
      else
        job_error "Error: cannot find BootEnv matching $BOOTENV"
        exit 1
      fi

      {{ else }}
      echo "Aborting: No bootenvs defined in ux://params/utility/bootstrap-bootenvs"
      exit 0
      {{ end }}
      echo "---------"

      {{ if (has "iso" $stage) }}
      echo "iso already set, no action"
      {{ else }}
      echo "setting bootstrap phase completed"
      drpcli profiles set global param "bootstrap/completed" to "{{ append $stage "iso" | uniq | toJson }}" > /dev/null
      {{ end }}

      echo "done"
      exit 0
