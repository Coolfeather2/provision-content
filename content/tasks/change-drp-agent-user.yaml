---
Name: change-agent-user
Description: Change the agent to use a different user.
Documentation: |
  This task brings up a new agent running under an admin account that is running on an unmanaged-context. The context doesn't exist, but allows for the new agent to run tasks.  It then creates and starts the new agent under the user, then stops the original agent, marking the job complete and stopping the pivot agent.

  !!! warn
  
      Running the agent as other than an admin account has consequences.  Most tasks are designed to use admin priviledges.  You will likely break things.
Templates:
  - Name: pivot-agent-start.sh
    Meta:
      OS: linux
    Contents: |
      #!/usr/bin/env bash
      {{ template "prelude.tmpl" . }}
      
      # TODO: Maybe track original context and switch to that instead of context.
      
      AGENT_NAME="drp-pivot-agent"
      CONTEXT_NAME="unmanaged-pivot-agent"

      {{ if (.ParamExists "drp-agent/username") }}
        auname={{ .ParamExpand "drp-agent/username" }}
        cuname=$(whoami)
      
        if [[ "$cuname" != "root" ]]; then
          privcmd="{{ .ParamExpand "drp-agent/privcmd" }}"
          job_info "User is not root.  Using \"$privcmd\"."
        fi

        if [[ "{{ .Machine.Context }}" != "$CONTEXT_NAME" ]]; then
          job_info "Cleaning up any errant $AGENT_NAME instances."
          $privcmd /usr/local/bin/drpcli agent stop --agent-name $AGENT_NAME 2> /dev/null || :
          $privcmd /usr/local/bin/drpcli agent remove --agent-name $AGENT_NAME 2> /dev/null || :
          $privcmd rm -rf /var/lib/$AGENT_NAME

          if [[ "$cuname" == "$auname" ]]; then
            job_success "Agent is running as \"$cuname\"."
          fi
          
          job_info "Starting $AGENT_NAME."
          $privcmd /usr/local/bin/drpcli agent install --agent-name $AGENT_NAME --agent-username=root --context "$CONTEXT_NAME"
          $privcmd /usr/local/bin/drpcli agent start --agent-name $AGENT_NAME
          # The order is reversed because of how inserttask works.  Tasklist will be in the reverse order of this.
          job_info "Updating TaskList to switch to $CONTEXT_NAME."
          drpcli machines inserttask {{ .Machine.Uuid }} "change-agent-user" 0 > /dev/null
          drpcli machines inserttask {{ .Machine.Uuid }} "context:$CONTEXT_NAME" 0 > /dev/null
          job_success "Started $CONTEXT_NAME"
        fi
 
        job_info "Removing original agent."
        $privcmd /usr/local/bin/drpcli agent stop
        $privcmd /usr/local/bin/drpcli agent remove
        $privcmd rm -rf /var/lib/drp-agent
        job_info "Configuring and starting agent to run under $auname."
        $privcmd /usr/local/bin/drpcli agent install --agent-username="$auname"
        $privcmd chown -R $auname /var/lib/drp-agent
        $privcmd /usr/local/bin/drpcli agent start --agent-username="$auname"
        job_info "Insert task to go back to original context."
        drpcli jobs update {{ .Machine.CurrentJob }} '{"State": "finished"}' > /dev/null
        drpcli machines update {{ .Machine.Uuid }} '{"Context":""}' > /dev/null
        job_info "Stopping $AGENT_NAME"
        $privcmd rm -rf /var/lib/$AGENT_NAME
        $privcmd /usr/local/bin/drpcli agent remove --agent-name $AGENT_NAME 2> /dev/null || :
        $privcmd /usr/local/bin/drpcli agent stop --agent-name $AGENT_NAME 2> /dev/null || :
      
        # This should never be hit because the previous line destroys the agent that would get the job_success.
        job_warn "Agent configured to run as \"$auname\", but you really shouldn't see this!"
      {{ else}}
        job_warn 'Param "drp-agent/username" is not defined.'
      {{ end }}
