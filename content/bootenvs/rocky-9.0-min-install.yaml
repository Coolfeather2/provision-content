---
Name: rocky-9.0-min-install
Description: Rocky 9 minimal installer that points to the latest Rocky 9 release
Documentation: |
  This BootEnv installs the Rocky 9.0 minimal operating system.

  ISOs can be downloaded from:

    * <https://download.rockylinux.org/pub/rocky/>
Meta:
  color: green
  feature-flags: change-stage-v2
  icon: linux
  title: Digital Rebar Community Content
  type: os
  group-by: Rocky
OptionalParams:
  - operating-system-disk
  - provisioner-default-password-hash
  - kernel-console
  - kernel-options
  - proxy-servers
  - select-kickseed
  - kickstart-base-packages
  - extra-packages
OnlyUnknown: false
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
  arm64-uefi: EFI/BOOT/BOOTAA64.EFI
OS:
  Name: rocky-9.0-min
  Family: redhat
  Codename: rocky
  Version: 9.0
  SupportedArchitectures:
    x86_64:
      IsoFile: Rocky-9.0-x86_64-minimal.iso
      Sha256: dbe7ee3000baa22ebfc7d66611597081c6b3e792a8c0c86a5a9d58e14650d874
      IsoUrl: https://dl.rockylinux.org/vault/rocky/9.0/isos/x86_64/Rocky-9.0-x86_64-minimal.iso
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    aarch64:
      IsoFile: Rocky-9.0-aarch64-minimal.iso
      Sha256: 807aa4a26f1182704fbbd620feff92eed4d974c7f84ef64a53243d63e9adb530
      IsoUrl: https://dl.rockylinux.org/vault/rocky/9.0/isos/aarch64/Rocky-9.0-aarch64-minimal.iso
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    ppc64le:
      IsoFile: Rocky-9.0-ppc64le-minimal.iso
      Sha256: c75e02fe6b522ee60f35da471adc4394b051bfc15dbe04f4966aebb2ad1538f6
      IsoUrl: https://dl.rockylinux.org/vault/rocky/9.0/isos/ppc64le/Rocky-9.0-ppc64le-minimal.iso
      Loader: boot/grub/powerpc-ieee1275/core.elf
      Kernel: ppc/ppc64/vmlinuz
      Initrds:
        - ppc/ppc64/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
Templates:
  - Name: kexec
    Path: "{{.Machine.Path}}/kexec"
    ID: kexec.tmpl
  - Name: pxelinux
    Path: "pxelinux.cfg/{{.Machine.HexAddress}}"
    ID: default-pxelinux.tmpl
  - Name: ipxe
    Path: "{{.Machine.Address}}.ipxe"
    ID: default-ipxe.tmpl
  - Name: pxelinux-mac
    Path: 'pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}'
    ID: default-pxelinux.tmpl
  - Name: ipxe-mac
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
    ID: default-ipxe.tmpl
  - Name: grub
    Path: "grub/{{.Machine.Address}}.cfg"
    ID: default-grub.tmpl
  - Name: grub-mac
    Path: 'grub/{{.Machine.MacAddr "grub"}}.cfg'
    ID: default-grub.tmpl
  - Name: grub-http-boot
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
    ID: default-grub.tmpl
  - Name: compute.ks
    Path: "{{.Machine.Path}}/compute.ks"
    ID: select-kickseed.tmpl
