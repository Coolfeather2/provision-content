---
Name: rocky-9.1-min-install
Description: Rocky 9 minimal installer that points to the latest Rocky 9 release
Documentation: |
  This BootEnv installs the Rocky 9.1 minimal operating system.

  ISOs can be downloaded from:

    * <https://download.rockylinux.org/pub/rocky/>
Meta:
  color: green
  feature-flags: change-stage-v2
  icon: linux
  title: Digital Rebar Community Content
  type: os
  group-by: Rocky
OptionalParams:
  - operating-system-disk
  - provisioner-default-password-hash
  - kernel-console
  - kernel-options
  - proxy-servers
  - select-kickseed
  - kickstart-base-packages
  - extra-packages
OnlyUnknown: false
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
  arm64-uefi: EFI/BOOT/BOOTAA64.EFI
OS:
  Name: rocky-9.1-min
  Family: redhat
  Codename: rocky
  Version: 9.1
  SupportedArchitectures:
    x86_64:
      IsoFile: Rocky-9.1-x86_64-minimal.iso
      Sha256: bae6eeda84ecdc32eb7113522e3cd619f7c8fc3504cb024707294e3c54e58b40
      IsoUrl: http://download.rockylinux.org/vault/rocky/9.1/isos/x86_64/Rocky-9.1-x86_64-minimal.iso
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    aarch64:
      IsoFile: Rocky-9.1-aarch64-minimal.iso
      Sha256: 16edc2842bc31f845cb665ee8333ec05032418d5c010489040031f62f14bacf5
      IsoUrl: http://download.rockylinux.org/vault/rocky/9.1/isos/aarch64/Rocky-9.1-aarch64-minimal.iso
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    ppc64le:
      IsoFile: Rocky-9.1-ppc64le-minimal.iso
      Sha256: 6f11bec41470cbbb24bf819ba02e4dca1e7667c06dc6e3a4d987e02f4f01eb29
      IsoUrl: http://download.rockylinux.org/vault/rocky/9.1/isos/ppc64le/Rocky-9.1-ppc64le-minimal.iso
      Loader: boot/grub/powerpc-ieee1275/core.elf
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
Templates:
  - Name: kexec
    Path: "{{.Machine.Path}}/kexec"
    ID: kexec.tmpl
  - Name: pxelinux
    Path: "pxelinux.cfg/{{.Machine.HexAddress}}"
    ID: default-pxelinux.tmpl
  - Name: ipxe
    Path: "{{.Machine.Address}}.ipxe"
    ID: default-ipxe.tmpl
  - Name: pxelinux-mac
    Path: 'pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}'
    ID: default-pxelinux.tmpl
  - Name: ipxe-mac
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
    ID: default-ipxe.tmpl
  - Name: grub
    Path: "grub/{{.Machine.Address}}.cfg"
    ID: default-grub.tmpl
  - Name: grub-mac
    Path: 'grub/{{.Machine.MacAddr "grub"}}.cfg'
    ID: default-grub.tmpl
  - Name: grub-http-boot
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
    ID: default-grub.tmpl
  - Name: compute.ks
    Path: "{{.Machine.Path}}/compute.ks"
    ID: select-kickseed.tmpl
