---
Name: alma-8-install
Description: AlmaLinux-8 installer that points to the latest Alma 8 release.
Documentation: |
  This BootEnv installs the AlmaLinux 8 DVD operating system.

  ISOs can be downloaded from:

    * <https://mirrors.almalinux.org/isos.html>

  The *DVD* ISO is nearly 10 GB in size.  This will require at least 30 GB of
  free disk space on the DRP Endpoint to be exploded out correctly.

Meta:
  color: blue
  feature-flags: change-stage-v2
  icon: linux
  title: Digital Rebar Community Content
  type: os
  group-by: Alma
OptionalParams:
  - operating-system-disk
  - provisioner-default-password-hash
  - kernel-console
  - kernel-options
  - proxy-servers
  - select-kickseed
  - kickstart-base-packages
  - extra-packages
OnlyUnknown: false
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
  arm64-uefi: EFI/BOOT/BOOTAA64.EFI
OS:
  Name: alma-8
  Family: redhat
  Codename: alma
  Version: "8.9"
  SupportedArchitectures:
    x86_64:
      IsoFile: AlmaLinux-8.9-x86_64-dvd.iso
      Sha256: f4d536fd1512fdf1936a8305454239bdcc9020f1b633d0ef37f1ff34a396b8d5
      IsoUrl: https://repo.almalinux.org/almalinux/8.9/isos/x86_64/AlmaLinux-8.9-x86_64-dvd.iso
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        ksdevice=bootif
        ks={{.Machine.Url}}/compute.ks
        method={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    aarch64:
      IsoFile: AlmaLinux-8.9-aarch64-dvd.iso
      Sha256: 5a4adb965beb173c936d14a48ebfbdfad72a13bd401d4ae4de0f11dd2a173bbe
      IsoUrl: https://repo.almalinux.org/almalinux/8.9/isos/aarch64/AlmaLinux-8.9-aarch64-dvd.iso
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        ksdevice=bootif
        ks={{.Machine.Url}}/compute.ks
        method={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    ppc64le:
      IsoFile: AlmaLinux-8.9-ppc64le-dvd.iso
      Sha256: 8feef4278f9808b8c2580c2f218194c67abe340655d0e94a76bbcf9f9a5c005a
      IsoUrl: https://repo.almalinux.org/almalinux/8.9/isos/ppc64le/AlmaLinux-8.9-ppc64le-dvd.iso
      Loader: "core.elf"
      Kernel: "ppc/ppc64/vmlinuz"
      Initrds:
        - "ppc/ppc64/initrd.img"
      BootParams: >-
        ksdevice=bootif
        ks={{.Machine.Url}}/compute.ks
        method={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
Templates:
  - Name: kexec
    Path: "{{.Machine.Path}}/kexec"
    ID: kexec.tmpl
  - Name: pxelinux
    Path: "pxelinux.cfg/{{.Machine.HexAddress}}"
    ID: default-pxelinux.tmpl
  - Name: ipxe
    Path: "{{.Machine.Address}}.ipxe"
    ID: default-ipxe.tmpl
  - Name: pxelinux-mac
    Path: 'pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}'
    ID: default-pxelinux.tmpl
  - Name: ipxe-mac
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
    ID: default-ipxe.tmpl
  - Name: grub
    Path: "grub/{{.Machine.Address}}.cfg"
    ID: default-grub.tmpl
  - Name: grub-mac
    Path: 'grub/{{.Machine.MacAddr "grub"}}.cfg'
    ID: default-grub.tmpl
  - Name: grub-http-boot
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
    ID: default-grub.tmpl
  - Name: compute.ks
    Path: "{{.Machine.Path}}/compute.ks"
    ID: select-kickseed.tmpl
