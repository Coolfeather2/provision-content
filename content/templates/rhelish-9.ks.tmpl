# DRP RHELish version 9 and newer kickstart template
text
%addon com_redhat_kdump --enable --reserve-mb='auto'
%end
keyboard --xlayouts='us'

{{if .ParamExists "redhat/kickstart-syspurpose" -}}
{{if .Param "redhat/kickstart-syspurpose" -}}
syspurpose {{range $key, $val := (.Param "redhat/kickstart-syspurpose") -}} --{{ $key }}="{{ $val }}" {{end}}
{{else -}}
# syspurpose is a RHEL Server 9 and newer only kickstart directive
# no syspurpose directive specified in Param redhat/kickstart-syspurpose
{{end -}}
{{end -}}

{{if .ParamExists "redhat/rhsm-organization" -}}
rhsm{{if .Param "redhat/subscription-server"}} --server-hostname={{.ParamExpand "redhat/subscription-server"}}{{end}} --organization="{{.Param "redhat/rhsm-organization"}}" --activation-key="{{.Param "redhat/rhsm-activation-key"}}" {{.Param "redhat/rhsm-additional"}}
{{else -}}
# rhsm is only supported in RHEL Server platforms, not derivative distros (eg rocky, alma, ol, etc)
# no RHSM org/key specified - skipping registration/activation process
{{end -}}

{{if .ParamExists "redhat/kickstart-shell" }}
{{if eq ( .Param "redhat/kickstart-shell") true}}
{{if .ParamExists "provisioner-access-key"}}
sshpw --username={{if .ParamExists "provisioner-default-user"}}{{.Param "provisioner-default-user"}}{{else}}rocketskates{{end}} --sshkey {{.Param "provisioner-access-key"}}

{{else -}}
sshpw --username={{if .ParamExists "provisioner-default-user"}}{{.Param "provisioner-default-user"}}{{else}}rocketskates{{end}} --iscrypted {{if .ParamExists "provisioner-default-password-     hash"}}{{.Param "provisioner-default-password-hash"}}{{else}}$6$TNfj2zjqvI5ICeDM$jMOazmEcMphCFQYd47sCmjtdIkXjI3eegqE17sydyRo7bjCVntZ9dE815KUYnnUCZnvjOdvYbSkK80lnnNgSM1{{end}}

{{end -}}
{{else -}}
# no SSH Shell username/key configured

{{end -}}
{{end -}}

{{ if .ParamExists "redhat/rhsm-organization" }}
# Not using install bits.
{{else}}
  {{range .InstallRepos}}
  {{ .Install }}
  {{end}}
{{end}}

rootpw --iscrypted {{if .ParamExists "provisioner-default-password-hash"}}{{.Param "provisioner-default-password-hash"}}{{else}}$6$drprocksdrprocks$upAIK9ynEEdFmaxJ5j0QRvwmIu2ruJa1A1XB7GZjrnYYXXyNr4qF9FttxMda2j.cmh.TSiLgn4B/7z0iSHkDC1{{end}}
skipx

{{if .ParamExists "part-scheme" -}}
{{$templateName := (printf "part-scheme-%s.tmpl" (.Param "part-scheme")) -}}
{{.CallTemplate $templateName .}}
{{else -}}
bootloader {{.ParamExpand "redhat-bootorder" }} --location=mbr --append="rhgb quiet"
zerombr
ignoredisk --only-use={{.Param "operating-system-disk"}}
clearpart --all --initlabel --drives={{.Param "operating-system-disk"}}
autopart --type=lvm
{{end -}}

reboot {{if .Param "kexec-ok" }}--kexec{{end}}

# Network information
network --hostname={{ if .ParamExists "hostname" }}{{ .Param "hostname" }}{{ else }}{{ .Machine.ShortName }}{{ end }}
{{ if .ParamExists "provisioner-network-config" }}network {{ .ParamExpand "provisioner-network-config" }}{{ end }}

%packages
{{template "kickstart-package-selections.tmpl" .}}
%end

%pre
{{ range $intf := .Param "kickstart/extra-ifs" }}
nmcli con add type ethernet con-name {{ $intf }} ifname {{ $intf }}
{{end}}
%end

%post

exec > /root/post-install.log 2>&1
set -x
export PS4='${BASH_SOURCE}@${LINENO}(${FUNCNAME[0]}): '

{{ if .ParamExists "kernel-options" -}}
grubby --update-kernel=ALL --args="{{ .ParamExpand "kernel-options" -}}
{{end -}}
{{ if .ParamExists "init-options" -}}
grubby --update-kernel=ALL --args="-- {{ .ParamExpand "init-options" -}}
{{end -}}

{{template "reset-workflow.tmpl" .}}
{{template "runner.tmpl" .}}

sync
%end
