---
Name: ansible-playbooks
Description: A task to invoke a specific set of ansible playbooks pulled from git
Documentation: |
  A task to invoke a specific set of ansible playbooks pulled from git.

  Sequence of operations (loops over all entries:

    1. collect args if provided
    1. git clone repo to name
    1. git checkout commit if provided
    1. cd to name & path
    1. run ansible-playbook playbook and args if provided
    1. remove the directories

  !!! note
      Requires Param `ansible/playbooks` - List of playbook git repos
      to run on the machines (either local or cluster machines).

Meta:
  color: black
  feature-flags: sane-exit-codes
  icon: "paper plane outline"
  title: RackN Content
RequiredParams:
  - ansible/playbooks
OptionalParams:
  - rsa/key-public
  - rsa/key-user
  - rsa/key-private
ExtraClaims:
  - scope: "machines"
    action: "*"
    specific: "*"
  - scope: "profiles"
    action: "*"
    specific: "*"
Templates:
  - Name: git-ansible-playbook
    Contents: |
      #!/usr/bin/env bash
      # Invoke a set of ansible playbooks pulled from git
      #

      {{template "setup.tmpl" .}}

      PKGS=""
      for WANT in git ansible
      do
        which $WANT > /dev/null 2>&1 || PKGS="${WANT} "
      done

      if [[ -n "$PKGS" ]]; then
          echo "Attempting to install missing packages:  $PKGS"
          install_lookup $PKGS
      fi

      ## This will test for local and force it to run the local playbook
      ## Otherwise if this is a cluster, try to build a multi-node inventory file
      {{ $machine := .Machine.Name -}}
      {{ $ac := .Param "ansible/connection-local"}}
      {{ $mc := get .Machine.Meta "machine-role" }}
      {{ if and (not $ac) (eq $mc "cluster") }}
        {{ $cl := $machine }}
        {{ $pmap := dict }}
        {{ $_ := set $pmap "root" $ }}
        {{ $_ := set $pmap "cluster" $cl }}
        {{ $_ := set $pmap "path" "hosts.ini" }}
        {{ .CallTemplate "cluster-ansible-playbook.tmpl" $pmap }}

        KEYARGS="--private-key=rsa-{{$machine}}"
      {{ else }}
        KEYARGS=""
        ## Pull data for playbook from Machine
        {{ if .Param "ansible/connection-local"}}
          ADDRESS="localhost"
        {{ else }}
          ## Pull data for playbook from Machine
          ADDRESS=$(drpcli machines show $RS_UUID | jq -r .Address)
          if [[ "$ADDRESS" == "" ]]; then
            ADDRESS="localhost"
          else
            ADDRESS="$ADDRESS"
            {{ if not (eq .Machine.Context "") }}
            KEYARGS="--private-key=rsa-{{$machine}}"
            {{ end }}
          fi
        {{ end }}

        cat >hosts.ini  <<EOF
      $ADDRESS ansible_connection=local
      EOF
      {{ end }}

      # we need a keypair for Ansible
      echo "Retrieving SSH key from Machine Params rsa/key-*"
      tee rsa-{{.Machine.Name}} >/dev/null << EOF
      {{if .ParamExists "rsa/key-private"}}{{.Param "rsa/key-private" | replace "|" "\n" }}{{end}}
      EOF
      chmod 600 rsa-{{.Machine.Name}}
      tee rsa-{{.Machine.Name}}.pub >/dev/null << EOF
      {{if .ParamExists "rsa/key-public"}}{{.Param "rsa/key-public"}}{{end}}
      EOF
      chmod 644 rsa-{{.Machine.Name}}.pub

      export ANSIBLE_HOST_KEY_CHECKING=False

      echo "Running ansible/playbooks"
      {{range $index, $playbook := .ParamExpand "ansible/playbooks"}}

          echo "======== Item {{$index}} of ansible/playbooks ========="
          NAME="{{if $playbook.name}}{{$playbook.name}}{{else}}playbook{{$index}}{{end}}"
          DIR="/{{if $playbook.path}}{{$playbook.path}}{{else}}{{end}}"
          PB="{{if $playbook.playbook}}{{$playbook.playbook}}{{else}}playbook{{$index}}.yaml{{end}}"
          REQUIREMENTS="{{if $playbook.requirements}}{{$playbook.requirements}}{{end}}"

          {{if $playbook.template }}
            echo "  Creating playbook ${NAME}/${PB} from DRP template: {{$playbook.template}}"
            mkdir ${NAME}
            tee ${NAME}/${PB} >/dev/null << EOF
      {{$.CallTemplate $playbook.template $}}
      EOF
          {{else}}
            {{ if not $playbook.repo}}
              echo "ERROR: template or repo must be defined!"
              exit 1
            {{else}}
              echo "  Cloning the git repo: {{$playbook.repo}}"
              git clone "{{$playbook.repo}}" "$NAME"
              {{if not $playbook.commit }}
                echo "  No commit requsted, using default commit"
              {{else}}
                echo "  Checking out {{$playbook.commit}}"
                git checkout {{$playbook.commit}}
              {{end}}
            {{end}}
          {{end}}

          {{if not $playbook.data }}
              echo "  No special args requsted"
              ARGS=""
          {{else}}
              echo "  Using args: {{$playbook.args}}"
              ARGS="{{$playbook.args}}"
          {{end}}


          cd "${NAME}${DIR}"

          ## handle ansible-galaxy requirements
          if [ -n "$REQUIREMENTS" ]; then
            if [ -f "$REQUIREMENTS" ]; then
              echo "  Running ansible-galaxy install -r $REQUIREMENTS"
              ansible-galaxy install -r  "$REQUIREMENTS"
            else
              echo "  ERROR: ansible-galaxy requirements file $REQUIREMENTS not found."
              exit 1
            fi 
          else
            echo "  No ansible-galaxy requirements file specified."
          fi

          echo "Making DRP machine JSON available to playbook"
          tee digitalrebar.json >/dev/null << EOF
      { digitalrebar: $(drpcli machines show $RS_UUID)
      {{- if $playbook.extravars -}},
        {{$.CallTemplate $playbook.extravars $}}
      {{ end -}}
      }
      EOF

          echo "  Running the playbook $PB in $(pwd)"
          ansible-playbook \
            -i host.ini \
            $KEYARGS \
            $ARGS {{if not $playbook.verbosity}}no_log: True{{end}} \
            --extra-vars @digitalrebar.json \
            "$PB"

          cd -
      {{else}}
          echo "NOTE: No ansible/playbooks defined."
      {{end}}
      echo "======== End of Loop ========="

      echo "done"
      exit 0
