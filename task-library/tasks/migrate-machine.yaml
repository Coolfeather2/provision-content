---
Name: migrate-machine
Description: Task to migrate machine to new DRP endpoint.
Documentation: |
  The `migrate-machine` task does the following:

    * Gathers machine information on the old endpoint.
    * Normally checks the contents and versions are the same.  This can be skipped with the `migrate-machine/skip-content-check` parameter.
    * Normally checks for profiles on the machine that don't exist and creates them on the new endpoint.  This can be skipped with the `migrate-machine\skip-profiles` parameter.
    * Creates a new machine with the same UUID.
    * Updates the machine's parameter
    * Updates the drp-agent config with new endpoint and machine token on the new endpoint.
    * Restarts the agent

  At this point the task restarts and catches the `migrate-machine/complete` is set to true and runs the following on the new endpoint:

    * Deletes all jobs related to the machine on the old endpoint.
    * Removes the machine from the old endpoint.
ExtraClaims:
  - scope: contents
    action: list
    specific: "*"
  - scope: info
    action: get
    specific: "*"
  - scope: machines
    action: delete
    specific: "*"
Meta:
  icon: truck
  color: red
  feature-flags: sane-exit-codes
Templates:
  - Name: machine-migrate
    Meta:
      OS: linux
    Contents: |-
      #!/bin/bash

      set -e

      {{template "setup.tmpl" .}}

      {{ if (.Param "migrate-machine/complete") -}}
      unset RS_LOCAL_PROXY
      old_endpoint={{ .Param "migrate-machine/old-endpoint-url" }}
      old_endpoint_token={{ .Param "migrate-machine/old-endpoint-token" }}
      (
        unset RS_LOCAL_PROXY
        export RS_ENDPOINT=$old_endpoint
        export RS_TOKEN=$old_endpoint_token
        drpcli machines deletejobs $RS_UUID
        drpcli machines destroy $RS_UUID
      )
      drpcli machines remove $RS_UUID param migrate-machine/old-endpoint-url
      drpcli machines remove $RS_UUID param migrate-machine/old-endpoint-token
      exit 0
      {{ end -}}
      # Mark complete early so we don't run migration on both endpoints.
      drpcli machines set $RS_UUID param migrate-machine/complete to true

      # Cannot migrate without these parameters.
      {{ if .ParamExists "migrate-machine/new-endpoint-url" }}
      new_endpoint="{{ .Param "migrate-machine/new-endpoint-url" }}"
      {{ else }}
      echo "\`migrate-machine/new-endpoint-url\' parameter not found."
      exit 1
      {{ end }}
      {{ if .ParamExists "migrate-machine/new-endpoint-token" }}
      new_endpoint_token="{{ .Param "migrate-machine/new-endpoint-token" }}"
      {{ else -}}
      echo "\`migrate-machine/new-endpoint-token\' parameter not found."
      exit 1
      {{ end -}}

      # Grab the old enpoint's machine token, confg, and parameters.  Drop `migrate-machine` profile.
      drpcli machines set $RS_UUID param migrate-machine/old-endpoint-url to \"$RS_ENDPOINT\" > /dev/null 2>&1
      drpcli machines set $RS_UUID param migrate-machine/old-endpoint-token to \"$RS_TOKEN\" > /dev/null 2>&1
      old_config=$(drpcli machines show --slim Params $RS_UUID | drpjq -c 'del(.Profiles[] | select(. == "migrate-machine"))')
      old_params=$(drpcli machines show --slim Meta $RS_UUID --decode | drpjq '.Params')

      # Only skip if there is no concern about content differences between endpoint.
      {{ if not (.Param "migrate-machine/skip-content-check") -}}
      if [[ $old_content != $new_content ]]; then
        old_content=$(drpcli contents list | drpjq -r '.[].meta | "\(.Name) \(.Version)"' | sort)
        new_content=$(unset RS_LOCAL_PROXY; RS_ENDPOINT=$new_endpoint RS_TOKEN=$new_endpoint_token drpcli contents list | drpjq -r '.[].meta | "\(.Name) \(.Version)"' | sort)
        echo ">>> Content and Version mismatch at:"
        echo "< = old endpoint     > = new endpoint"
        echo "-------------------------------------"
        diff <(echo "$old_content") <(echo "$new_content")
        echo "-------------------------------------"
        exit 1
      fi
      {{ end -}}

      # This will create profiles that do not exist for the machine on the new endpoint.  Do not create `machine-migrate` profile on the new endpoint.
      {{ if not (.Param "migrate-machine/skip-profiles") -}}
      old_machine_profiles=$(echo $old_config | drpjq -r '.Profiles[]')
      new_profiles=$(unset RS_LOCAL_PROXY; RS_ENDPOINT=$new_endpoint RS_TOKEN=$new_endpoint_token drpcli profiles list | drpjq -r '.[].Name')
      for old_machine_profile in $old_machine_profiles; do
        re=\\$old_machine_profile\\b
        if [[ ! $new_profiles =~ $re ]]; then
          old_profile=$(drpcli profiles show $old_profle --decode)
          (unset RS_LOCAL_PROXY; RS_ENDPOINT=$new_endpoint RS_TOKEN=$new_endpoint_token drpcli profiles create "$old_profile" > /dev/null 2>&1)
        fi
      done
      {{ end -}}

      # Create and update the new machine and agent.
      (
        unset RS_LOCAL_PROXY
        export RS_ENDPOINT=$new_endpoint
        export RS_TOKEN=$new_endpoint_token
        drpcli machines create "$old_config"
        drpcli machines params $RS_UUID "$old_params"
        new_token=$(drpcli machines token $RS_UUID --ttl 3y | drpjq -r '.Token')
      )
      sed -i "/^Endpoints/c\Endpoints: \"$new_endpoint\"" /var/lib/drp-agent/agent-cfg.yml
      sed -i "/^Token/c\Token: \'$new_endpoint_token\'" /var/lib/drp-agent/agent-cfg.yml
      { sleep 1 ; systemctl restart drp-agent; } &
      exit 16
