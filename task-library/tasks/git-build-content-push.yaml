---
Name: "git-build-content-push"
Description: "Pull content from git, build the content, and publish"
Documentation: |
  This task is used to automatically import and synchronize content packages
  from a git repository.  The process DOES NOT assume anything about the content;
  consequently, the repository MUST have scripts to facilitate building and
  uploading the content.

  When used with the `git-lab-trigger-webhook-push` trigger, the repository param
  will be automatically populated when `MergeData` is set to true.  This allows
  the webhook data to automically flow into the blueprint.

  !!! note
      At this time, the repository cannot require authentication.

  The process is:

    1. clone the git repository named in `params` value
    1. run the `tools/build_content.sh` script
    1. optionally add the content to the system (if `manager/update-catalog` is true)
    1. optionally rebuild the manager catalogs (if dr-server/update-content` is true)

  This assumes that the output of the content script will go into a directory
  structure like.

      rebar-catalog/<cp-name>/<version>.json

Meta:
  icon: "money"
  color: "yellow"
  title: "Digital Rebar Community Content"
  copyright: "RackN 2022"
  feature-flags: "sane-exit-codes"
  flow-comment: Installs Content from Git
RequiredParams:
  - repository
  - manager/update-catalog
  - dr-server/update-content
ExtraClaims:
  - scope: "contents"
    action: "*"
    specific: "*"
  - scope: "files"
    action: "*"
    specific: "*"
  - scope: "preferences"
    action: "*"
    specific: "*"
  - scope: "plugins"
    action: "*"
    specific: "*"
Templates:
  - Name: "build_content.backup"
    Path: "build_content.backup"
    Contents: |
      #!/usr/bin/env bash
      echo "> this script is part of git-build-content-push <"
      answer=`pwd`
      echo "starting content build for $answer"
      cp=$(basename $answer)
      cpname="${cp}"
      if [[ -e tools/version.sh ]] ; then
        version=$(tools/version.sh)
        echo "version $version"
      fi
      if [[ -z $version ]]; then
        version="v0.1"
      fi
      echo "using version $version"

      cd content
      drpcli contents bundle ../${cpname}.json Version=$version
      drpcli contents bundle ../${cpname}.yaml Version=$version --format=yaml
      cd ..

      mkdir -p rebar-catalog/${cpname}
      cp ${cpname}.json rebar-catalog/${cpname}/$version.json
      echo "created catalog: rebar-catalog/${cpname}/$version.json"

      echo "end"
  - Name: "git-build-content-push.sh"
    Contents: |
      #!/usr/bin/env bash

      set -e
      {{template "setup.tmpl" .}}

      echo "Make sure we have git"
      if ! which git 2>/dev/null >/dev/null ; then
          install git || :
      fi

      # GREG: creds here.
      {{ $repodata := .Param "repository" }}
      {{ $repo := get $repodata "git_http_url" }}
      cp=$(basename {{$repo}} .git)
      echo "Cloning {{$repo}}"
      git clone {{$repo}} $cp

      if [[ ! -e $cp/tools/build_content.sh ]] ; then
        echo "NOTE: repo did not include tools/build_content - using backup"
        mkdir -p $cp/tools/
        mv build_content.backup $cp/tools/build_content.sh
      fi

      cd $cp
      chmod +x tools/build_content.sh
      echo "running {{$repo}} tools/build_content.sh"
      echo "==================== START tools/build_content.sh ================================"
      tools/build_content.sh
      echo "==================== END tools/build_content.sh ================================"

      UpdateManager="false"
      {{ if .Param "manager/update-catalog" }}
      echo "yes, update manager (if manager)"
      UpdateManager=$(drpcli prefs list | jq .manager -r)
      echo "This is DRP a manager: $UpdateManager"
      {{ else }}
      echo "note: manager/update-catalog:false"
      {{ end }}

      UploadContent="false"
      {{ if .Param "dr-server/update-content" }}
      echo "yes, update content"
      UploadContent="true"
      {{ else }}
      echo "WARNING: NOT UPDATING CONTENT (dr-server/update-content=false)"
      {{ end }}

      find rebar-catalog -type f | while read f ; do
          echo "--------------- found $f ---------------"
          if [[ "$UpdateManager" == "true" ]] ; then
              echo "  Copy files into manager as $f"
              drpcli files upload $f as $f > /dev/null
          fi
          if [[ "$UploadContent" == "true" ]] ; then
              echo "  Upload content pack $f"
              CP=$(drpcli contents upload $f)
              NAME=$(jq -rc .meta.Name <<< "$CP")
              echo "  Create INFO alert that content $NAME was updated"
          fi
      done
      echo "uploads complete"

      if [[ "$UpdateManager" == "true" ]] ; then
          echo "Create catalog ... (long process)"
          drpcli plugins runaction manager buildCatalog
      fi

      cd ..
      echo "done"
      exit 0
