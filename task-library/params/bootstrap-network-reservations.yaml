---
Name: bootstrap-network-reservations
Description: "Network Subnet objects to install during bootstrap"
Documentation: |
  This Param can be set to an array of Reservation type objects.  If it is
  added to the `bootstrap-network` profile, then during the bootstrap
  Stage, the defined reservations will be added to the system.

  The easiest way to generate a valid reservation set of objects is to create the
  reservation on an existing DRP Endpoint, then show the JSON structure.  An
  example CLI command reservation named `192.168.124.101` is shown below:

    ```sh
    drpcli reservations show 192.168.124.101
    ```

  Multiple Subnets may be specified (eg `drpcli reservations list`; to dump
  all reservations on a given DRP Endpoint).  Each Subnet will be added to
  the system if they do not exist already.

  Example structures of two reservations is defined below.  NOTE that
  in real world use, more options or values may need to be set to make
  the subnet operationally correct for a given environment.

  YAML:
    ```yaml
    bootstrap-network-reservations:
      - Addr: 192.168.124.222
        Token: "01:02:03:04:05:00"
      - Addr: 192.168.124.223
        Token: "01:02:03:04:05:01"
    ```

  JSON:
    ```json
    [
      { "Addr": "192.168.124.222", "Token": "01:02:03:04:05:00" },
      { "Addr": "192.168.124.223", "Token": "01:02:03:04:05:01" }
    ]
    ```

  Generally speaking this process is used for initial DRP Endpoint installation with
  the `install.sh` installation script.  Preparing the Subnets and Reservations
  in either a Content Pack or a Profile that is applied to the system at installation
  time will enable the creation of the Objects.


  Example installation script invocation that would use this bootstrap process, assumes
  that `network.json` is provided by the operator with the network Subnet and/or
  Reservations definitions:

    ```sh
    # you must create "network.json" which should be a valid Profile object, containing
    # the appropriate Params and object information in each param as outlined in the
    # examples in the bootstrap-network-subnets and boostrap-network-reservations
    # Params "Documentation" field
    curl -fsSL get.rebar.digital/stable | bash -s -- install --universal --initial-profiles="./network.json"
    ```

Meta:
  color: blue
  icon: sitemap
  title: RackN Content
Schema:
  type: array
  items:
    type: object
