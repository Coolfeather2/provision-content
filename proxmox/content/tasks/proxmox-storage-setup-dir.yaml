---
Name: proxmox-storage-setup-dir
Description: "Setup the directory type storage on a Proxmox system."
Documentation: |
  This task is injected in to a running workflow via the Stage `proxmox-create-storage`.
  You must set the Param `proxmox/flexiflow-create-storage` to include this task
  name for it to be added to the system.

  This Task creates the `dir` storage to an existing Proxmox VE server if it doesn't
  yet exist.  Note that default Storage type is `lvm-thin`; which uses a full block
  device.  This type allows use of a directory to back the VM and Container images.

  The `proxmox/storage-config` Param defines the configuration to use for all storage
  types, including *dir* type.

  An example configuration for this task:

    ```yaml
    proxmox/storage-config:
      dir:
        local-images:
          path: /var/lib/images
          content: images,iso
        local:
          path: /var/lib/vz
          content: iso,vztmpl,backup
        backup:
          path: /mnt/backup
          content: backup
          maxfiles: 7
    ```

  This creates 3 directory structures for storing different content types in the
  existing filesystem.  For documentation on configuration values, please see:

      * <https://pve.proxmox.com/wiki/Storage>

  Config values in YAML/JSON stanzas match the Proxmox configuration values.

Meta:
  color: blue
  feature-flags: sane-exit-codes
  icon: expand arrows alternate
  title: RackN Content
OptionalParams:
  - proxmox/storage-config
  - proxmox/storage-device
  - proxmox/storage-name
  - proxmox/storage-skip-if-exists
  - rs-debug-enable
Prerequisites: []
Templates:
  - Name: "proxmox-create-storage"
    Contents: |-
      #!/usr/bin/env bash
      # Proxmox VE Storage create task for 'dir' type storage setup

      function xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "FATAL: $*\n"; exit $XIT; }

      set -e
      [[ "{{ .Param "rs-debug-enable" }}" == "true" ]] && set -x || true

      PVENODE=$(hostname)
      SKIP="{{ .Param "proxmox/storage-skip-if-exists" }}"

      {{ range $key, $val := ( .ParamCompose "proxmox/storage-config" ) -}}
      {{ if eq $key "dir" -}}
      {{ range $valkey, $valcfg := $val -}}
      echo ">>> 'dir' type storage config section found with name"
      NAME="{{ $valkey }}"
      CONTENT={{ if $valcfg.content }}"{{ $valcfg.content }}"{{ else }}"rootdir,images"{{ end }}
      CONTENT=$(echo "$CONTENT" | tr -d ' ')
      PATH_DIR={{ if $valcfg.path }}"{{ $valcfg.path }}"{{ else }}/var/lib/pve-storage{{ end }}
      FORMAT={{ if $valcfg.format }}"{{ $valcfg.format }}"{{ end }}
      PRUNE={{ if get $valcfg "prune-backups" }}"{{ get $valcfg "prune-backups" }}"{{ end }}
      MAXFILES={{ if $valcfg.maxfiles }}"{{ $valcfg.maxfiles }}"{{ end }}
      MAXFILES=$(echo "$MAXFILES" | tr -d ' ')

      echo ">>> processing 'dir' storage config for entry '{{ $valkey }}', with values:"
      echo "    +-------------------------------------------------------------------------"
      echo "    |       pvenode :: $PVENODE"
      echo "    |          name :: $NAME"
      echo "    |          path :: $PATH_DIR"
      echo "    |       content :: $CONTENT"
      echo "    |        format :: $FORMAT"
      echo "    | prune backups :: $PRUNE"
      echo "    |      maxfiles :: $MAXFILES"
      echo "    +-------------------------------------------------------------------------"
      echo ""

      [[ "$NAME" == "local" ]] && xiterr 1 "'local' is a reserved 'dir' type and can not be specified, remove it from config." || true

      [[ ! -d "$PATH_DIR" ]] && { echo ">>> Creating dir '$PATH_DIR'"; mkdir -p "$PATH_DIR"; } || true
      [[ -z "$NAME" ]] && xiterr 1 "No storage 'name' configuration specified."

      if pvesm list $NAME > /dev/null 2>&1
      then
        if [[ "$SKIP" == "true" ]]
        then
          xiterr 1 "Storage of type 'dir' with name '$NAME' exists already"
        else
          echo ">>> Exists is fatal is set to 'false' "
          echo ">>> Storage 'dir' name '$NAME' exists already, add/configure operations skipped."
        fi
      else
        echo ">>> Adding dir pool with name '$NAME' to '$PVENODE'"
        pvesm add dir $NAME -path "$PATH_DIR"

        # should we add a Param to allow trying to configure even if it exists already ?

        echo ">>> Starting storage configurations"
        [[ -n "$FORMAT" ]] && pvesm set $NAME --format $FORMAT || echo "--- No image formats specified, using defaults"
        [[ -n "$CONTENT" ]] && pvesm set $NAME --content $CONTENT || echo "--- No content types specified, using defaults"
        [[ -n "$MAXFILES" ]] && pvesm set $NAME -maxfiles $MAXFILES || echo "--- No maxfiles specified"
        [[ -n "$PRUNE" ]] && set $NAME -prune-backups $MAXFILES || echo "--- No prune backups specified"
      fi
      {{ end -}}
      {{ end -}}
      {{ end -}}
