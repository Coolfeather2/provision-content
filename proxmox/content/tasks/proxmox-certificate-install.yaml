---
Name: proxmox-certificate-install
Description: "Install HTTPS certificate for Proxmox API and Web GUI."
Documentation: |
  Installs an HTTPS certificate for the Proxmox API and Web GUI.  Does
  not update/change the Intra-Cluster certificates as these are self
  signed via internal CA.

  The certificate must be in PEM format, and the contents of the
  signed certificate stored in the Param `proxmox/certificate-crt`.
  The Private Key must be stored in the contents of the Param
  `proxmox/certificate-key`.

Meta:
  color: blue
  feature-flags: sane-exit-codes
  icon: shield alternate
  title: RackN Content
OptionalParams:
  - proxmox/certificate-force
  - proxmox/certificate-crt
  - proxmox/certificate-key
Templates:
  - Name: "proxmox-certificate-install.sh"
    Contents: |-
      #!/usr/bin/env bash
      # Proxmox HTTPS Certificate installation for API and Web GUI

      {{ template "setup.tmpl" . }}

      {{ if and ( .ParamExists "proxmox/certificate-crt" ) ( .ParamExists "proxmox/certificate-key" ) -}}
      job_info "Starting Proxmox HTTPS certificate install..."
      {{ else -}}
      job_info "No CRT and KEY defined; exiting certificate install task"
      echo "See the Params 'proxmox/certificate-crt' and 'proxmox/certificate-key'."
      exit 0
      {{ end -}}

      for FILE in /etc/pve/local/pveproxy-ssl.crt /etc/pve/local/pveproxy-ssl.key
      do
        if [[ -f "$FILE" ]]
        then
          STAMP=$(date +%y%m%d-%H%M%S)
          echo "Backing up existing file '$FILE' to '$FILE.bkup'"
          cp $FILE $FILE.bkup.$STAMP
        fi
      done

      {{ if .Param "proxmox/certificate-force" -}}
      echo "Force install of certificates (proxmox/certificate-force = true)..."
      FORCE="--force 1"
      {{ end -}}

      {{ if .ParamExists "proxmox/certificate-crt" -}}
      cat > certificate.crt << EO_PEM
      {{   .Param "proxmox/certificate-crt" }}
      EO_PEM
      {{ else -}}
      job_error "Missing certificate PEM in 'proxmox/certificate-crt'"
      {{ end -}}

      {{ if .ParamExists "proxmox/certificate-key" -}}
      cat > certificate.key << EO_KEY
      {{   .Param "proxmox/certificate-key" }}
      EO_KEY
      {{ else -}}
      job_error "Missing certificate Private Key in 'proxmox/certificate-key'"
      {{ end -}}

      job_info "Installing certificates..."

      pvenode cert set certificate.crt certificate.key $FORCE

      echo "Newly installed certificate information..."
      pvenode cert info

      echo "Restarting API and Web GUI (pveproxy) services..."
      systemctl restart pveproxy.service

      rm -f certificate.crt certificate.key

      job_success "Completed certificate installation..."
