The vmware-lib related dockerfiles for building Context containers has been
moved to the rackn/containers repository, at:

https://gitlab.com/rackn/containers

Context container builds are now handled automatically by the RackN build
services, and Context container images are staged in the RackN S3 bucket,
at:

https://get.rebar.digital

Full image file URL references are provided in the DRP Content bundles
that contain the Context object definitions.


